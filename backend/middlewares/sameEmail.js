let User = require("../models/User");

const isSameEmail = async (req, res, next) => {
  const { email } = await req.body;
  const checkEmail = await User.findOne({ email: email });
  if (checkEmail) {
    res.status(400).json({ message: "email is already in use" });
  } else {
    next();
  }
};
module.exports = {
  isSameEmail,
};

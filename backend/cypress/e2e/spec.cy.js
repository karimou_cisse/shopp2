describe("My First Test", () => {
  // signup
  it("Visits signup page", () => {
    cy.visit("http://localhost:3000/signup");
    cy.get("input[name='firstName']").type("john");
    cy.wait(500);
    cy.get("input[name='lastName']").type("Doe");
    cy.wait(500);
    cy.get("input[name='email']").type("john@example.com");
    cy.wait(500);
    cy.get("input[name='password']").type("azerty123");
    cy.wait(500);
    cy.get("button[type='submit']").click();
  });

  // login page
  it("Visits login page", () => {
    cy.visit("http://localhost:3000/login");
    cy.wait(1000);
    cy.get("input[name='email']").type("john@example.com");
    cy.wait(500);
    cy.get("input[name='password']").type("azerty123");
    cy.wait(500);
    cy.get("button[type='submit']").click();
  });

  // Men products page
  it("Visits men products page", () => {
    cy.visit("http://localhost:3000/products/Man");
    cy.wait(1000);
    cy.get(
      "img[src='https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/ba65b84e-8236-42e1-a3a1-1396a0f4460e/chaussure-dunk-low-pour-fhBsJ7.png']"
    ).click();
    cy.wait(500)
  });

  // product page
  it("Visits men products page", () => {
    cy.visit("http://localhost:3000/product/64620d75a94041e77fa88e20");
  });
});

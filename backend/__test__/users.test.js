const request = require("supertest");
const { expect } = require("chai");
const dotenv = require("dotenv");
dotenv.config();
const { app, server } = require("../index");
const User = require("../models/User");

before(function (done) {
  this.timeout(3000);
  setTimeout(done, 2000);
});
const newUser = {
  firstName: "John",
  lastName: "Doe",
  email: "john@example.com",
  password: "azerty12345",
};

describe("POST users", () => {
  it("should register new user with valid credentials", (done) => {
    request(app)
      .post("/auth/signup")
      .send(newUser)
      .expect(201)
      .then((res) => {
        expect(res.body.email).to.be.eql(newUser.email);
        done();
      })
      .catch((err) => done(err));
  });

  it("shouldn't accept the email that already exists in the database", (done) => {
    request(app)
      .post("/auth/signup")
      .send(newUser)
      .expect(400)
      .then((res) => {
        expect(res.body.message).to.be.eql("email is already in use");
        done();
      })
      .catch((err) => done(err));
  });

  it("Connect the user with vakid credentials", (done) => {
    request(app)
      .post("/auth/login")
      .send({ email: newUser.email, password: newUser.password })
      .expect(200)
      .then((res) => {
        expect(res.body.email).to.be.eql(newUser.email);
        done();
      })
      .catch((err) => done(err));
  });
});

after(async () => {
  try {
    await User.deleteOne({ email: newUser.email });
  } catch (err) {
    console.error(err);
  }
});

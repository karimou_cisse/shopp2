const request = require("supertest");
const { app, server } = require("../index");
const Product = require("../models/Product");

describe("Test de la route Product", () => {
  jest.setTimeout(10000);
  let fakeProduct;

  beforeAll(async () => {
    fakeProduct = new Product({
      name: "Nike test",
      price: 149,
      brand: "Nike",
      size: ["39", "40", "41", "42", "43", "44", "45", "46", "47"],
      types: [
        {
          color: "black",
          image:
            "https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/12218601-6303-49ca-8611-180dc1115e46/chaussure-air-vapormax-plus-pour-s3X2J0.png",
        },
      ],
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially ",
      categories: ["Woman"],
    });
    await fakeProduct.save();
  });

  afterAll(async () => {
    // await fakeProduct.remove();
    server.close();
  });

  it("devrait renvoyer un produit existant", async () => {
    const response = await request(app).get(`/products/${fakeProduct._id}`);
    expect(response.statusCode).toBe(200);
  });

  it("devrait renvoyer une erreur si le produit n'existe pas", async () => {
    const response = await request(app).get("/products/id-inexistant");
    expect(response.statusCode).toBe(500);
  });
  it("devrait supprimer le produit", async () => {
    const response = await request(app).delete(`/products/${fakeProduct._id}`);
    expect(response.statusCode).toBe(200);
  });
});
